import React from 'react';

// Object for testing purposes.
// Contains arrays of strings and objects, and nested objects.
const myArray = [
    {
        name: 'Jane Doe',
        birthYear: 1986,
        favFood: [
            {
                food: 'pizza'
            },
            {
                food: 'hot dog'
            }
        ],
        favoriteColors: [ 'red', 'orange' ],
        clothing: {
            top: {
              type: 'shirt',
              color: 'red'
            },
            bottom: {
              type: 'shorts',
              color: 'black'
            }
          }
      },
      {
        name: 'John Doe',
        birthYear: 1988,
        favFood: [
            {
                food: 'hamburger'
            },
            {
                food: 'steak'
            }
        ],
        favoriteColors: [ 'blue', 'yellow' ],
        clothing: {
          top: {
            type: 'shirt',
            color: 'red'
          },
          bottom: {
            type: 'shorts',
            color: 'black'
          }
        }
      }
]


// objects = Array of Objects
// function merges them into one
const merge = (objects) => {
    let out = {};

    for (let i = 0; i < objects.length; i++) {
        for (let obj in objects[i]) {
            out[obj] = objects[i][obj]
        }
    }

    // console.log('MERGE: ' + JSON.stringify(out));
    return out;
}

// function takes a nested object, makes it single-level
const flatten = (obj, name, stem) => {
    let out = {};
    let newStem = (typeof stem !== 'undefined' && stem !== '') ? stem + '_' + name : name;

    if (typeof obj !== 'object') {
        out[newStem] = obj;
        return out;
    }

    for (let o in obj) {
        let prop = flatten(obj[o], o, newStem);
        out = merge([out, prop])
    }

    console.log('FLATTEN: ' + JSON.stringify(out));
    return out;
}

const DataTable = (array) => {
    // maps over array of objects
    // key is array index of object, data is flattened object
    let rows = myArray.map((obj, i) => {
        return <Row key={i} data={flatten(obj)} />
    });

    // use keys from unflattened object to create table header
    let header = Object.keys(myArray[0]).map((field, i) => {
        return <th key={i}>{field}</th>
    })

    return (
        <table>
            <tbody>
                <tr>{header}</tr>
                {rows}
            </tbody>
        </table>
    )

}

// value in table cells come from flattened object.
// called inside DataTable function.
const Row = (props) => {
    return (
        <tr>
            <td style={{border: '1px solid black'}}>{props.data.name}</td>
            <td style={{border: '1px solid black'}}>{props.data.birthYear}</td>
            <td style={{border: '1px solid black'}}>{`${props.data.favFood_0_food}, ${props.data.favFood_1_food}`}</td>
            <td style={{border: '1px solid black'}}>{`${props.data.favoriteColors_0}, ${props.data.favoriteColors_1}`}</td>
            <td style={{border: '1px solid black'}}>
                {
                    `Top: ${props.data.clothing_top_color} ${props.data.clothing_top_type}, 
                    Bottom: ${props.data.clothing_bottom_color} ${props.data.clothing_bottom_type}`
                }
            </td>
        </tr>
    )
}

class App extends React.Component {
    render() {
        return (
            <div>
                {DataTable(myArray)}
            </div>
        )
    }
}

export default App;
